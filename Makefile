CC		=	cc
CFLAGS	=	-O2

BIN		=	mcdsubcpu_dec

MAIN	=	main.o

DECOMP	=	decomp.o

OBJS	=	$(MAIN) \
			$(DECOMP)

$(BIN): $(OBJS)
	$(CC) -o $(BIN) $(OBJS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf $(BIN) $(OBJS)
