/*
 * mcdsubcpu_dec - decompressor for MegaCD SubCPU BIOS objects
 * Copyright (C) 2021  Matt Gilmore
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "decomp.h"

#define BUFFER_SIZE 0x10000

int main(int argc, char *argv[])
{
    FILE *f_in, *f_out;
    uint8_t *p_src, p_buffer[BUFFER_SIZE];
    size_t src_size, src_read, buffer_size = BUFFER_SIZE;

    if (argc != 3) {
        fputs("Usage: decomp infile.bin outfile.bin\n", stderr);
        return EXIT_FAILURE;
    }

    f_in = fopen(argv[1], "rb");

    fseek(f_in, 0, SEEK_END);
    src_size = (size_t) ftell(f_in);
    fseek(f_in, 0, SEEK_SET);

    p_src = malloc(src_size);
    src_read = fread(p_src, 1, src_size, f_in);
    fclose(f_in);
    if (src_read != src_size) {
        free(p_src);
        fputs("error reading source file, unexpected read size\n", stderr);
        return EXIT_FAILURE;
    }

    buffer_size = decomp(p_buffer, buffer_size, p_src);
    free(p_src);

    f_out = fopen(argv[2], "wb");
    fwrite(p_buffer, 1, buffer_size, f_out);
    fclose(f_out);

    return EXIT_SUCCESS;
}
