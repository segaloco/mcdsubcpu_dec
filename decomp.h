/*
 * decomp - the decompressor routine for SubCPU BIOS objects
 * Copyright (C) 2021  Matt Gilmore
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DECOMP_H
#define DECOMP_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stddef.h>
#include <stdint.h>

/* decomp - decompress src into dest up to dest_size, return amount decompressed */
extern size_t decomp(uint8_t *dest, size_t dest_size, uint8_t *src);

#ifdef __cplusplus
};
#endif /* __cplusplus */

#endif /* DECOMP_H */
