/*
 * decomp - the decompressor routine for SubCPU BIOS objects
 * Copyright (C) 2021  Matt Gilmore
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <stdint.h>

#define get_next_word(word,src,remaining_bits)      word = *((int16_t *) src); \
                                                    src += 2; \
                                                    remaining_bits = 16

#define get_next_bit(bit,word,src,remaining_bits)   bit = (word & 1) == 1; \
                                                    word >>= 1; \
                                                    if (--remaining_bits == 0) { \
                                                        get_next_word(word, src, remaining_bits); \
                                                    } 

size_t decomp(uint8_t *dest, size_t dest_size, uint8_t *src)
{
    uint8_t *dest_base;
    int16_t word;
    int remaining_bits;

    dest_base = dest;

    get_next_word(word, src, remaining_bits);

    while ((size_t) (dest - dest_base) <= dest_size) {
        char bit;

        get_next_bit(bit, word, src, remaining_bits);

        if (bit) {
            *(dest++) = *(src++);
        } else {
            int string_size;
            int16_t string_index;
            uint8_t byte_00, byte_01;

            string_size = 0;
            string_index = (int16_t) 0xFF00;

            get_next_bit(bit, word, src, remaining_bits);

            if (!bit) {
                get_next_bit(bit, word, src, remaining_bits);
                string_size = bit;

                get_next_bit(bit, word, src, remaining_bits);
                string_size = (string_size << 1) | bit;
                string_size++;

                string_index |= *(src++);
            } else {
                byte_00 = *(src++);
                byte_01 = *(src++);

                string_index |= byte_01;                    
                string_index <<= 5;
                string_index &= 0xFF00;
                string_index |= byte_00;

                if ((byte_01 &= 0x7))
                    string_size = byte_01 + 1;
            }

            if (!string_size) {
                byte_01 = *(src++);

                if (byte_01 == 0)
                    break;
                else if (byte_01 != 1)
                    string_size = byte_01;
                else
                    continue;
            }

            do {
                byte_00 = dest[string_index];
                *(dest++) = byte_00;
            } while (string_size-- > 0);
        }
    }

    return (size_t) (dest - dest_base);
}
